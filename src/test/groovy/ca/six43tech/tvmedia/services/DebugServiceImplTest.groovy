package ca.six43tech.tvmedia.services

import spock.lang.Specification
import spock.lang.Unroll

class DebugServiceImplTest extends Specification {
    static private final List PROPS = [DebugServiceImpl.SIX43_JSON_CAP_PROP, DebugServiceImpl.SIX43_JSON_CAP_DIR_PROP, DebugServiceImpl.SIX43_DEBUG_OVERRIDE_PROP]
    static private final Map PROP_VALS = [:]

    def setupSpec() {
        PROPS.each {
            PROP_VALS[it] = System.getProperty(it)
        }
    }

    def cleanupSpec() {
        PROP_VALS.each { k, v ->
            if(v != null)
                System.setProperty(k, v)
            else
                System.clearProperty(k)
        }
    }

    @Unroll
    def 'isJsonCaptureEnabled() returns #expected when property value is #value'() {
        given:
            if(value != null)
                System.setProperty(DebugServiceImpl.SIX43_JSON_CAP_PROP, value)
            else
                System.clearProperty(DebugServiceImpl.SIX43_JSON_CAP_PROP)
        expect:
            new DebugServiceImpl().jsonCaptureEnabled == expected
        where:
            value   || expected
            'true'  || true
            'True'  || true
            'trUe'  || true
            '1'     || false
            '0'     || false
            'FALSE' || false
            null    || false
    }

    def 'isJsonCaptureEnabled() returns true when enableJsonCapture() is called'() {
        given:
            System.clearProperty(DebugServiceImpl.SIX43_JSON_CAP_PROP)
            assert new DebugServiceImpl().jsonCaptureEnabled == false
        when:
            new DebugServiceImpl().enableJsonCapture()
        then:
            new DebugServiceImpl().jsonCaptureEnabled
    }

    def 'isJsonCaptureEnabled() returns false when disableJsonCapture() is called'() {
        given:
            System.setProperty(DebugServiceImpl.SIX43_JSON_CAP_PROP, 'true')
            assert new DebugServiceImpl().jsonCaptureEnabled == true
        when:
            new DebugServiceImpl().disableJsonCapture()
        then:
            new DebugServiceImpl().jsonCaptureEnabled == false
    }

    def 'capture() writes to a file'() {
        when:
            def f = new DebugServiceImpl().capture('foo')
        then:
            f.exists()
            f.text == 'foo'
        cleanup:
            f.delete()
    }

    @Unroll
    def 'isDebugOverrideEnabled() returns #expected when property value is #value'() {
        given:
        if(value != null)
            System.setProperty(DebugServiceImpl.SIX43_DEBUG_OVERRIDE_PROP, value)
        else
            System.clearProperty(DebugServiceImpl.SIX43_DEBUG_OVERRIDE_PROP)
        expect:
            new DebugServiceImpl().debugOverrideEnabled == expected
        where:
            value   || expected
            'true'  || true
            'True'  || true
            'trUe'  || true
            '1'     || false
            '0'     || false
            'FALSE' || false
            null    || false
    }
}
