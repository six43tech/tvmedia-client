package ca.six43tech.tvmedia.services

import ca.six43tech.tvmedia.models.Team
import spock.lang.Specification

class JsonConverterServiceImplTest extends Specification {

    def 'convertToObject() returns an object'() {
        expect:
            new JsonConverterServiceImpl().convertToObject('{}', Team) instanceof Team
    }

    def 'convertToArray() returns an array'() {
        expect:
            new JsonConverterServiceImpl().convertToArray('[{}]', Team).class.array
    }
}
