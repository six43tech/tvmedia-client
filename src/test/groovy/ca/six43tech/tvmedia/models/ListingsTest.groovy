package ca.six43tech.tvmedia.models

import ca.six43tech.tvmedia.TvMediaClient
import spock.lang.Specification
import spock.lang.Unroll

class ListingsTest extends Specification {
    def 'The given TvMediaClient is returned when requested'() {
        given:
            def clnt = Mock(TvMediaClient)
            def airings = []
        when:
            def listings = new Listings(airings, clnt)
        then:
            listings.tvMediaClient.is(clnt)
    }

    @Unroll
    def 'All airings are initialized with the tvmedia client <#numberOfAirings airings>'() {
        given:
            def clnt = Mock(TvMediaClient)
            def airings = []
            (1..numberOfAirings).each {
                airings << Mock(Airing) {
                    1 * setTvMediaClient(clnt)
                    0 * _
                }
            }
        and:
            new Listings(airings, clnt)
        where:
            numberOfAirings << [0, 1, 2, 20, 100]
    }
}
