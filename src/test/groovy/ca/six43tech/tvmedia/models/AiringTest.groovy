package ca.six43tech.tvmedia.models

import spock.lang.Specification
import spock.lang.Unroll

class AiringTest extends Specification {
    @Unroll
    def 'Team objects are created as expected based on provided team data'() {
        when:
            def a = new Airing()
            a._team1ID = team1ID
            a._team2ID = team2ID
            a._team1Name = team1Name
            a._team2Name = team2Name
        then:
            a.teams.size() == expectedSize
        where:
            team1ID | team2ID   | team1Name | team2Name || expectedSize
            0       | 0         | null      | null      || 0
            1       | 1         | null      | null      || 0
            0       | 0         | 'a'       | 'b'       || 0
            1       | 0         | 'a'       | null      || 1
            0       | 1         | null      | 'a'       || 1
            1       | 1         | 'a'       | null      || 1
            1       | 1         | null      | 'a'       || 1
            0       | 1         | 'a'       | null      || 0
            1       | 1         | 'a'       | 'b'       || 2
    }
}
