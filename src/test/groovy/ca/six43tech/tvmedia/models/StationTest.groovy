package ca.six43tech.tvmedia.models

import ca.six43tech.tvmedia.TvMediaClient
import ca.six43tech.tvmedia.test.TestUtils
import spock.lang.Specification
import spock.lang.Unroll

class StationTest extends Specification {
    @Unroll
    def 'The number of airings in the returned listings matches the number of airings returned by the api <#responseSize Airings>'() {
        given:
            def clnt = Mock(TvMediaClient) {
                1 * getResponse(*_)
                1 * convertArrayResponse(*_) >> { TestUtils.arrayOfObjects(responseSize, Airing) as Airing[] }
                1 * getApiKey()
                0 * _
            }
            def station = new Station(1, 'a', 'b', 'c', 'd', null, null, 'e', 'f', true)
        when:
            station.tvMediaClient = clnt
            def listings = station.getListings()
        then:
            listings.iterator().size() == responseSize
        where:
            responseSize << [0, 1, 10, 100]
    }
}
