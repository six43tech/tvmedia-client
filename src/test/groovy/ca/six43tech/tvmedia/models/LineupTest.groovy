package ca.six43tech.tvmedia.models

import ca.six43tech.tvmedia.TvMediaClient
import ca.six43tech.tvmedia.test.TestUtils
import spock.lang.Specification
import spock.lang.Unroll

class LineupTest extends Specification {
    @Unroll
    def 'The number of airings in the returned listings matches the number of airings returned by the api <#responseSize Airings>'() {
        given:
            def clnt = Mock(TvMediaClient) {
                1 * getResponse(*_)
                1 * convertArrayResponse(*_) >> { TestUtils.arrayOfObjects(responseSize, Airing) as Airing[] }
                1 * getApiKey()
                0 * _
            }
            def lineup = new Lineup()
        when:
            lineup.tvMediaClient = clnt
            def listings = lineup.getListings()
        then:
            listings.iterator().size() == responseSize
        where:
            responseSize << [0, 1, 10, 100]
    }

    @Unroll
    def 'isLazyLoadDone() returns #expected when expected'() {
        given:
            def lineup = new Lineup()
        when:
            lineup.channels = channels
            lineup.country = country
        then:
            lineup.isLazyLoadDone() == expected
        where:
            channels| country   || expected
            null    | null      || false
            []      | null      || false
            null    | []        || false
            []      | []        || true
    }

    def 'lazyLoad() only loads from network once'() {
        given:
            def mockLineup = Mock(Lineup)
            mockLineup.channels >> []
            mockLineup.country >> 'CA'
        and:
            def clnt = Mock(TvMediaClient) {
                1 * getLineup(*_) >> { mockLineup }
            }
            def lineup = new Lineup()
            lineup.tvMediaClient = clnt
        and:
            lineup.provider
            lineup.channels
            lineup.country
    }
}
