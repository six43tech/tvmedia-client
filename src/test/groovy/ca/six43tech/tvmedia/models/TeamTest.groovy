package ca.six43tech.tvmedia.models

import spock.lang.Specification
import spock.lang.Unroll

class TeamTest extends Specification {
    @Unroll
    def 'The assigned id is returned when #id is used'() {
        when:
            def t = new Team(id, "")
        then:
            t.id == id
        where:
            id << [Integer.MIN_VALUE, -1, 0, 1, 10000, Integer.MAX_VALUE]
    }

    @Unroll
    def 'The assigned name is returned when \'#name\' is used'() {
        when:
            def t = new Team(0, name)
        then:
            t.name == name
        where:
            name << [null, '', 'a', 'b', 'abcdefg']
    }
}
