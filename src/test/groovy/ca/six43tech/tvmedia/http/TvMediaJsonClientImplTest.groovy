package ca.six43tech.tvmedia.http

import ca.six43tech.tvmedia.http.options.ApiKeyOption
import ca.six43tech.tvmedia.services.DebugService
import org.apache.http.client.fluent.Executor
import spock.lang.Specification

class TvMediaJsonClientImplTest extends Specification {
    def 'When the base URL is invalid an exception is throw'() {
        when:
            new TvMediaJsonClientImpl('abc', ':3js\\/jk3', Mock(Executor), Mock(DebugService))
        then:
            def e = thrown(RuntimeException)
            e.cause instanceof MalformedURLException
    }

    def 'When the base URL is not http/s an exception is thrown'() {
        when:
            new TvMediaJsonClientImpl('abc', 'ftp://foo.com', Mock(Executor), Mock(DebugService))
        then:
            def e = thrown(RuntimeException)
            e.cause instanceof MalformedURLException
    }

    def 'The default URL does not throw an exception'() {
        when:
            new TvMediaJsonClientImpl('abc')
        then:
            noExceptionThrown()
    }

    def 'When a request option is repeated an exception is thrown'() {
        given:
            def a = new ApiKeyOption('x')
            def b = new ApiKeyOption('y')
        when:
            def clnt = new TvMediaJsonClientImpl('a')
            clnt.getResponse('foo', TvMediaJsonClient.Action.GET, a, b)
        then:
            thrown(IllegalArgumentException)
    }

    def 'When the debug service overrides debug mode, it is honoured'() {
        given:
            def dbgSvc = Mock(DebugService)
            dbgSvc.debugOverrideEnabled >> true
            def clnt = new TvMediaJsonClientImpl('abc', TvMediaJsonClientImpl.DEFAULT_URL, Mock(Executor), dbgSvc)
            clnt.disableDebugMode()
        expect:
            clnt.@debugMode == false
            clnt.debugMode == true
    }
}
