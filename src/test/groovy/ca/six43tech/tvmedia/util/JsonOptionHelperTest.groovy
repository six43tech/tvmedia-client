package ca.six43tech.tvmedia.util

import ca.six43tech.tvmedia.http.options.JsonRequestOption
import spock.lang.Specification

class JsonOptionHelperTest extends Specification {
    def 'generateOptions() returns the expected sized array'() {
        given:
            def array = new JsonRequestOption[5]
            Arrays.fill(array, Mock(JsonRequestOption))
            def opts = new JsonRequestOption[41]
            Arrays.fill(opts, Mock(JsonRequestOption))
        expect:
            JsonOptionHelper.generateOptions(array, opts).length == array.length + opts.length
    }
}
