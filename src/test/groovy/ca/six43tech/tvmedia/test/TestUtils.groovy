package ca.six43tech.tvmedia.test

import ca.six43tech.tvmedia.models.JsonApiObject
import spock.mock.DetachedMockFactory

class TestUtils {
    static arrayOfObjects(int size, Class<? extends JsonApiObject> clazz) {
        def factory = new DetachedMockFactory()
        def list = []
        for(int i = 0; i < size; ++i)
            list << factory.Mock(clazz)
        list
    }
}
