package ca.six43tech.tvmedia

import ca.six43tech.tvmedia.http.TvMediaJsonClient
import ca.six43tech.tvmedia.models.Lineup
import ca.six43tech.tvmedia.models.Station
import ca.six43tech.tvmedia.models.Team
import ca.six43tech.tvmedia.services.JsonConverterService
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Unroll

class TvMediaClientTest extends Specification {

    @Shared private TvMediaClient clnt
    @Shared private TvMediaJsonClient jsonClnt
    @Shared private JsonConverterService jsonConverterService

    def setup() {
        jsonClnt = Mock(TvMediaJsonClient)
        jsonConverterService = Mock(JsonConverterService)
        clnt = new TvMediaClient('abc', false, jsonClnt, jsonConverterService)
    }

    @Unroll
    def 'getLineupsForZipCode() interacts properly for response size of #arraySize'() {
        given:
            def respArray = new Lineup[arraySize]
            def jsonObj = Mock(Lineup)
            Arrays.fill(respArray, jsonObj)
        when:
            clnt.getLineupsForZipCode('abc')
        then:
            1 * jsonClnt.getResponse(*_)
            1 * jsonConverterService.convertToArray(*_) >> respArray
            respArray.size() * jsonObj.setTvMediaClient(_)
            0 * _
        where:
            arraySize << [0, 3, 40]
    }

    @Unroll
    def 'getGenericLineups() interacts properly for response size of #arraySize'() {
        given:
            def respArray = new Lineup[arraySize]
            def jsonObj = Mock(Lineup)
            Arrays.fill(respArray, jsonObj)
        when:
            clnt.getGenericLineups()
        then:
            1 * jsonClnt.getResponse(*_)
            1 * jsonConverterService.convertToArray(*_) >> respArray
            respArray.size() * jsonObj.setTvMediaClient(_)
            0 * _
        where:
            arraySize << [0, 3, 40]
    }

    def 'getLineup() interacts properly'() {
        given:
            def jsonObj = Mock(Lineup)
        when:
            clnt.getLineup('abc')
        then:
            1 * jsonClnt.getResponse(*_)
            1 * jsonConverterService.convertToObject(*_) >> jsonObj
            1 * jsonObj.setTvMediaClient(_)
            0 * _
    }

    def 'getStation() interacts properly'() {
        given:
        def jsonObj = Mock(Station)
        when:
        clnt.getStation('abc')
        then:
        1 * jsonClnt.getResponse(*_)
        1 * jsonConverterService.convertToObject(*_) >> jsonObj
        1 * jsonObj.setTvMediaClient(_)
        0 * _
    }

    def 'getResponse() interacts properly'() {
        when:
            clnt.getResponse('a', TvMediaJsonClient.Action.GET)
        then:
            1 * jsonClnt.getResponse(*_)
            0 * _
    }

    @Unroll
    def 'convertResponse() interacts properly'() {
        when:
            clnt.convertResponse('', Team)
        then:
            1 * jsonConverterService.convertToObject(*_) >> result
            0 * _
        where:
            result << [null, new Team(0, '')]
    }

    @Unroll
    def 'convertArrayResponse() interacts properly'() {
        when:
            clnt.convertArrayResponse('', Team)
        then:
            1 * jsonConverterService.convertToArray(*_) >> result
            0 * _
        where:
            result << [null, [new Team(0, '')] as Team[]]
    }
}
