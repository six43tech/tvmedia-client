package ca.six43tech.tvmedia.util;

import ca.six43tech.tvmedia.http.options.JsonRequestOption;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class JsonOptionHelper {
    static public JsonRequestOption[] generateOptions(JsonRequestOption[] array, JsonRequestOption... extraOptions) {
        ArrayList<JsonRequestOption> options = new ArrayList<>();
        if(array != null)
            for(JsonRequestOption jro : array)
                options.add(jro);
        if(extraOptions != null)
            for(JsonRequestOption jro : extraOptions)
                options.add(jro);
        return options.toArray(new JsonRequestOption[options.size()]);
    }

    static public <T extends JsonRequestOption> T[] generateOptions(JsonRequestOption[] array, Class<T> clazz, JsonRequestOption... extraOptions) {
        JsonRequestOption[] allOptions = generateOptions(array, extraOptions);
        List<T> castedOptions = new ArrayList<>();
        for(JsonRequestOption jro : allOptions)
            castedOptions.add((T)jro);
        return castedOptions.toArray((T[])Array.newInstance(clazz, 0));
    }
}
