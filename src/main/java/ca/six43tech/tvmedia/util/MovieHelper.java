package ca.six43tech.tvmedia.util;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.Map;

public class MovieHelper {

    static public String extractCastFromMovie(String json) {
        Gson gson = new Gson();
        Type stringStringMap = new TypeToken<Map<String, Object>>(){}.getType();
        Map<String,String> map = gson.fromJson(json, stringStringMap);
        return gson.toJson(map.get("cast"));
    }

    private MovieHelper() {}
}
