package ca.six43tech.tvmedia.models;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class Airing extends JsonApiObject {
    @SerializedName("listingID") private int id;
    @SerializedName("listDateTime") private Date start;
    private int duration; // minutes
    @SerializedName("showID") private int showId;
    @SerializedName("seriesID") private int seriesId;
    private String showName;
    private String episodeTitle;
    private String episodeNumber; // next to useless
    @SerializedName("parts") private int totalParts;
    @SerializedName("partNum") private int partNumber;
    private boolean seriesPremiere;
    private boolean seasonPremiere;
    private boolean seriesFinale;
    private boolean seasonFinale;
    private boolean repeat;
    @SerializedName("new") private boolean isNew;
    private String rating;
    private boolean captioned;
    private boolean educational;
    @SerializedName("blackWhite") private boolean blackAndWhite;
    private boolean subtitled;
    private boolean live;
    private boolean hd;
    private boolean descriptiveVideo;
    private boolean inProgress;
    private String showTypeId;
    private int breakoutLevel;
    private String showType;
    private String year; // why not int??
    private String guest;
    private String cast;
    private String director;
    private int starRating;
    private String description;
    @SerializedName("league") private String leagueId;
    private transient Team[] teams = new Team[0];
    private String event;
    private String location;
    private String showPicture;
    // TODO: figure out format of this array
    //private Object[] artwork;
    private String showHost;
    private transient Channel channel;

    @SerializedName("team1ID") private int _team1ID;
    @SerializedName("team2ID") private int _team2ID;
    @SerializedName("team1") private String _team1Name;
    @SerializedName("team2") private String _team2Name;

    @SerializedName("number") private String _chanNumber;
    @SerializedName("channelNumber") private int _chanMajor;
    @SerializedName("subChannelNumber") private int _chanMinor;
    @SerializedName("stationID") private int _stationId;
    @SerializedName("name") private String _chanName;
    @SerializedName("callsign") private String _chanCallsign;
    @SerializedName("network") private String _chanNetwork;
    @SerializedName("stationType") private String _chanType;
    @SerializedName("webLink") private String _chanWebLink;
    @SerializedName("logoFilename") private String _chanLogo;


    public Team[] getTeams() {
        if(teams.length == 0) {
            Team t1 = null;
            Team t2 = null;
            int numTeams = 0;
            if(_team1Name != null && _team1Name.length() > 0 && _team1ID > 0) {
                t1 = new Team(_team1ID, _team1Name);
                ++numTeams;
                _team1Name = null;
            }
            if(_team2Name != null && _team2Name.length() > 0 && _team2ID > 0) {
                t2 = new Team(_team2ID, _team2Name);
                ++numTeams;
                _team2Name = null;
            }
            teams = new Team[numTeams];

            if(t1 != null)
                teams[0] = t1;
            else if(t2 != null)
                teams[0] = t2;

            if(t2 != null && t1 != null)
                teams[1] = t2;
        }
        return teams;
    }

    public Channel getChannel() {
        if(channel == null)
            channel = new Channel(_stationId, _chanName, _chanCallsign, _chanNetwork, _chanType, null, null, _chanWebLink, _chanLogo, null, _chanNumber, _chanMajor, _chanMinor);
        return channel;
    }

    public int getId() {
        return id;
    }

    public Date getStart() {
        return start;
    }

    public int getDuration() {
        return duration;
    }

    public int getShowId() {
        return showId;
    }

    public int getSeriesId() {
        return seriesId;
    }

    public String getShowName() {
        return showName;
    }

    public String getEpisodeTitle() {
        return episodeTitle;
    }

    public String getEpisodeNumber() {
        return episodeNumber;
    }

    public int getTotalParts() {
        return totalParts;
    }

    public int getPartNumber() {
        return partNumber;
    }

    public boolean isSeriesPremiere() {
        return seriesPremiere;
    }

    public boolean isSeasonPremiere() {
        return seasonPremiere;
    }

    public boolean isSeriesFinale() {
        return seriesFinale;
    }

    public boolean isSeasonFinale() {
        return seasonFinale;
    }

    public boolean isRepeat() {
        return repeat;
    }

    public boolean isNew() {
        return isNew;
    }

    public String getRating() {
        return rating;
    }

    public boolean isCaptioned() {
        return captioned;
    }

    public boolean isEducational() {
        return educational;
    }

    public boolean isBlackAndWhite() {
        return blackAndWhite;
    }

    public boolean isSubtitled() {
        return subtitled;
    }

    public boolean isLive() {
        return live;
    }

    public boolean isHd() {
        return hd;
    }

    public boolean isDescriptiveVideo() {
        return descriptiveVideo;
    }

    public boolean isInProgress() {
        return inProgress;
    }

    public String getShowTypeId() { return showTypeId; }

    public int getBreakoutLevel() {
        return breakoutLevel;
    }

    public String getShowType() {
        return showType;
    }

    public String getYear() {
        return year;
    }

    public String getGuest() {
        return guest;
    }

    public String getCast() {
        return cast;
    }

    public String getDirector() {
        return director;
    }

    public int getStarRating() {
        return starRating;
    }

    public String getDescription() {
        return description;
    }

    public String getLeagueId() {
        return leagueId;
    }

    public String getEvent() {
        return event;
    }

    public String getLocation() {
        return location;
    }

    public String getShowPicture() {
        return showPicture;
    }

    public String getShowHost() {
        return showHost;
    }

    @Override
    public String toString() {
        return "Airing{" +
                "id=" + id +
                ", start=" + start +
                ", showName='" + showName + '\'' +
                ", episodeTitle='" + episodeTitle + '\'' +
                "} ";
    }
}
