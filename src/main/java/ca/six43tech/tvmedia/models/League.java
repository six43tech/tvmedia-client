package ca.six43tech.tvmedia.models;

import ca.six43tech.tvmedia.TvMediaClient;
import ca.six43tech.tvmedia.http.TvMediaJsonClient;
import ca.six43tech.tvmedia.http.options.*;
import ca.six43tech.tvmedia.util.JsonOptionHelper;
import com.google.gson.annotations.SerializedName;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.TimeZone;

public class League extends NonLazyJsonApiObject {

    @SerializedName("leagueID") private String id;
    @SerializedName("leagueName") private String name;
    private String logo;

    public League(String id, String name, String logo) {
        this.id = id;
        this.name = name;
        this.logo = logo;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getLogo() {
        return logo;
    }

    public Team[] getTeams() throws IOException {
        TvMediaClient clnt = getTvMediaClient();
        return clnt.convertArrayResponse(clnt.getResponse(String.format("leagues/%s/teams", id), TvMediaJsonClient.Action.GET, new ApiKeyOption(clnt.getApiKey())), Team.class);
    }

    public Listings getListings(Lineup lineup, ListingsFetchRequestOption... options) throws IOException {
        TvMediaClient clnt = getTvMediaClient();
        List<JsonRequestOption> list = new ArrayList<>(Arrays.asList(options));
        list.add(new TimeZoneOption(TimeZone.getDefault()));
        JsonRequestOption[] allOptions = JsonOptionHelper.generateOptions(list.toArray(new JsonRequestOption[0]), new ApiKeyOption(clnt.getApiKey()), new LineupOption(lineup.getId()));
        Airing[] airings = clnt.convertArrayResponse(clnt.getResponse(String.format("leagues/%s/listings", id), TvMediaJsonClient.Action.GET, allOptions), Airing.class);
        return new Listings(Arrays.asList(airings), clnt);
    }

    public Listings getListingsQuickly(Lineup lineup, ListingsFetchRequestOption... options) throws IOException {
        ListingsFetchRequestOption[] allOptions = JsonOptionHelper.generateOptions(options, ListingsFetchRequestOption.class, new DisplayArtworkOption(false));
        return getListings(lineup, allOptions);
    }

    @Override
    public String toString() {
        return "League{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", logo='" + logo + '\'' +
                '}';
    }
}
