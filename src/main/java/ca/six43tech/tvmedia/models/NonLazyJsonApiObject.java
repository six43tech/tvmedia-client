package ca.six43tech.tvmedia.models;

abstract public class NonLazyJsonApiObject extends JsonApiObject {
    @Override
    protected boolean isLazyLoadDone() { return true; }
}
