package ca.six43tech.tvmedia.models;

public class Channel extends Station {
    private String number;
    private int channelNumber;
    private Integer subChannelNumber;

    public String getNumber() {
        return number;
    }

    public int getChannelNumber() {
        return channelNumber;
    }

    public int getSubChannelNumber() {
        return subChannelNumber;
    }

    public Channel(int id, String name, String callsign, String network, String stationType, Integer ntscTSID, Integer dtvTSID, String webLink, String logo, Boolean isHD, String number, int channelNumber, Integer subChannelNumber) {
        super(id, name, callsign, network, stationType, ntscTSID, dtvTSID, webLink, logo, isHD);
        this.number = number;
        this.channelNumber = channelNumber;
        this.subChannelNumber = subChannelNumber;
    }

    @Override
    public String toString() {
        return "Channel{" +
                "number='" + number + '\'' +
                ", channelNumber=" + channelNumber +
                ", subChannelNumber=" + subChannelNumber +
                "} " + super.toString();
    }
}
