package ca.six43tech.tvmedia.models;

public class SportsGenre extends Genre {
    public SportsGenre(String id, String name) {
        super(id, name);
    }
}
