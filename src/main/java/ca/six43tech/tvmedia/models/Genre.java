package ca.six43tech.tvmedia.models;

import com.google.gson.annotations.SerializedName;

abstract public class Genre extends NonLazyJsonApiObject {

    @SerializedName("genreID") private final String id;
    @SerializedName("genre") private final String name;

    public Genre(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Genre{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
