package ca.six43tech.tvmedia.models;

import ca.six43tech.tvmedia.TvMediaClient;

abstract public class JsonApiObject {

    private transient TvMediaClient client;

    public void setTvMediaClient(TvMediaClient client) {
        this.client = client;
    }

    public TvMediaClient getTvMediaClient() {
        return client;
    }

    protected void lazyLoad() {}

    protected boolean isLazyLoadDone() { return false; }
}
