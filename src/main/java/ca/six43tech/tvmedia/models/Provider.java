package ca.six43tech.tvmedia.models;

public class Provider extends NonLazyJsonApiObject {
    private final int id;
    private final String name;

    Provider(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Provider{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
