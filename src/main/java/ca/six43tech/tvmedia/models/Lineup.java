package ca.six43tech.tvmedia.models;

import ca.six43tech.tvmedia.TvMediaClient;
import ca.six43tech.tvmedia.http.TvMediaJsonClient;
import ca.six43tech.tvmedia.http.options.*;
import ca.six43tech.tvmedia.util.JsonOptionHelper;
import com.google.gson.annotations.SerializedName;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.TimeZone;

public class Lineup extends JsonApiObject {
    @SerializedName("lineupID") private String id;
    @SerializedName("lineupName") private String name;
    @SerializedName("lineupType") private String type;
    private transient Provider provider;
    private String serviceArea;
    private String country;
    @SerializedName("stations") private Channel[] channels;

    @SerializedName("providerID") private Integer _providerID;
    @SerializedName("providerName") private String _providerName;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public Provider getProvider() {
        lazyLoad();
        return provider;
    }

    public String getServiceArea() {
        return serviceArea;
    }

    public String getCountry() {
        lazyLoad();
        return country;
    }

    public Channel[] getChannels() {
        lazyLoad();
        return channels;
    }

    @Override
    public void setTvMediaClient(TvMediaClient tvMediaClient) {
        super.setTvMediaClient(tvMediaClient);
        if(channels != null)
            for(Channel c : channels)
                c.setTvMediaClient(tvMediaClient);
    }

    public Listings getListings(ListingsFetchRequestOption... options) throws IOException {
        TvMediaClient clnt = getTvMediaClient();
        TimeZoneOption tzo = new TimeZoneOption(TimeZone.getDefault());
        List<JsonRequestOption> list = new ArrayList<>(Arrays.asList(options));
        list.add(tzo);
        JsonRequestOption[] allOptions = JsonOptionHelper.generateOptions(list.toArray(new JsonRequestOption[0]), new ApiKeyOption(clnt.getApiKey()));
        Airing[] airings = clnt.convertArrayResponse(clnt.getResponse(String.format("lineups/%s/listings", id), TvMediaJsonClient.Action.GET, allOptions), Airing.class);
        return new Listings(Arrays.asList(airings), clnt);
    }

    public Listings getListingsQuickly(ListingsFetchRequestOption... options) throws IOException {
        return getListings(JsonOptionHelper.generateOptions(options, ListingsFetchRequestOption.class, new DisplayArtworkOption(false)));
    }

    @Override
    protected void lazyLoad() {
        if(!isLazyLoadDone()) {
            TvMediaClient clnt = getTvMediaClient();
            Lineup l;
            try {
                l = clnt.getLineup(id);
            } catch(IOException e) {
                throw new RuntimeException("Error lazy loading Lineup", e);
            }
            channels = l.getChannels();
            for(Channel c : channels)
                c.setTvMediaClient(clnt);
            country = l.getCountry();
            if (provider == null && _providerID != null && _providerName != null) {
                provider = new Provider(_providerID, _providerName);
                _providerID = null;
                _providerName = null;
            }
        }
    }

    @Override
    protected boolean isLazyLoadDone() {
        return channels != null && country != null;
    }

    @Override
    public String toString() {
        return "Lineup{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", type='" + type + '\'' +
                ", provider=" + getProvider() +
                ", serviceArea='" + serviceArea + '\'' +
                ", country='" + getCountry() + '\'' +
                ", channels=" + Arrays.toString(getChannels()) +
                '}';
    }
}
