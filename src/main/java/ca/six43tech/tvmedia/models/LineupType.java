package ca.six43tech.tvmedia.models;

public enum LineupType {
    CAB,
    IPTV,
    OTA,
    SAT
}
