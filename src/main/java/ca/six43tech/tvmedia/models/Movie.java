package ca.six43tech.tvmedia.models;

import ca.six43tech.tvmedia.TvMediaClient;
import ca.six43tech.tvmedia.http.TvMediaJsonClient;
import ca.six43tech.tvmedia.http.options.*;
import ca.six43tech.tvmedia.util.JsonOptionHelper;
import ca.six43tech.tvmedia.util.MovieHelper;
import com.google.gson.annotations.SerializedName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

public class Movie extends JsonApiObject {
    static private final Logger LOG = LoggerFactory.getLogger(Movie.class);

    @SerializedName("movieID") private int id;
    private String title;
    @SerializedName("titleSort") private String sortedTitle;
    private String alternateTitle;
    private String description;
    private String editorialDescription;
    private String featureDescription;
    private boolean noStarRating;
    private int starRating;
    private int duration; // minutes
    private transient MovieGenre genre;
    private String subGenre;
    private String year;
    private String releaseDate;
    private String rating;
    @SerializedName("USRating") private String usRating;
    @SerializedName("MPAARating") private String mpaaRating;
    @SerializedName("MPAAReason") private String mpaaReason;
    @SerializedName("imdbID") private String imdbId;
    private String officialWebsite;
    @SerializedName("trailerURL") private String trailerUrl;
    private String productionCompany;
    private Boolean captioned;
    private Boolean educational;
    @SerializedName("blackWhite") private Boolean blackAndWhite;
    private Boolean subtitled;
    private String picture; // image
    private Cast[] processedCast;
    // TODO: figure out format of this array
    //private Object[] artwork;

    @SerializedName("genreID") private String _genreId;
    @SerializedName("genre") private String _genreName;
    private transient boolean castLoaded = false;

    @Override
    protected boolean isLazyLoadDone() {
        return processedCast != null || castLoaded;
    }

    @Override
    protected void lazyLoad() {
        if(!isLazyLoadDone()) {
            castLoaded = true;
            TvMediaClient clnt = getTvMediaClient();
            try {
                String json = clnt.getResponse(String.format("movies/%s", id), TvMediaJsonClient.Action.GET, new ApiKeyOption(clnt.getApiKey()));
                processedCast = clnt.convertArrayResponse(MovieHelper.extractCastFromMovie(json), Cast.class);
            } catch (IOException e) {
                throw new RuntimeException(String.format("Error lazy loading Movie [%s]", id), e);
            }
        }
    }

    public Cast[] getCast() {
        lazyLoad();
        return processedCast;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getSortedTitle() {
        lazyLoad();
        return sortedTitle;
    }

    public String getAlternateTitle() {
        lazyLoad();
        return alternateTitle;
    }

    public String getDescription() {
        return description;
    }

    public String getEditorialDescription() {
        return editorialDescription;
    }

    public String getFeatureDescription() {
        return featureDescription;
    }

    protected boolean isNoStarRating() {
        return noStarRating;
    }

    public Integer getStarRating() {
        return isNoStarRating() ? null : starRating;
    }

    public int getDuration() {
        return duration;
    }

    public MovieGenre getGenre() {
        if(genre == null && _genreId != null && _genreName != null) {
            genre = new MovieGenre(_genreId, _genreName);
        }
        return genre;
    }

    public String getSubGenre() {
        return subGenre;
    }

    public String getYear() {
        return year;
    }

    public Date getReleaseDate() {
        if(releaseDate == null || "0000-00-00".equals(releaseDate))
            return null;
        try {
            return new SimpleDateFormat("yyyy-MM-dd").parse(releaseDate);
        } catch(Throwable t) {
            LOG.error(String.format("Unparsable release date found [%s]", releaseDate), t);
            return null;
        }
    }

    public String getRating() {
        return rating;
    }

    public String getUsRating() {
        return usRating;
    }

    public String getMpaaRating() {
        return mpaaRating;
    }

    public String getMpaaReason() {
        lazyLoad();
        return mpaaReason;
    }

    public String getImdbId() {
        return imdbId;
    }

    public String getOfficialWebsite() {
        return officialWebsite;
    }

    public String getTrailerUrl() {
        return trailerUrl;
    }

    public String getProductionCompany() {
        return productionCompany;
    }

    public boolean isCaptioned() {
        return captioned;
    }

    public boolean isEducational() {
        return educational;
    }

    public boolean isBlackAndWhite() {
        return blackAndWhite;
    }

    public boolean isSubtitled() {
        return subtitled;
    }

    public String getPicture() {
        return picture;
    }

    public Listings getListings(Lineup lineup, ListingsFetchRequestOption... options) throws IOException {
        TvMediaClient clnt = getTvMediaClient();
        List<JsonRequestOption> list = new ArrayList<>(Arrays.asList(options));
        list.add(new TimeZoneOption(TimeZone.getDefault()));
        JsonRequestOption[] allOptions = JsonOptionHelper.generateOptions(list.toArray(new JsonRequestOption[0]), new ApiKeyOption(clnt.getApiKey()), new LineupOption(lineup.getId()));
        Airing[] airings = clnt.convertArrayResponse(clnt.getResponse(String.format("movies/%s/listings", id), TvMediaJsonClient.Action.GET, allOptions), Airing.class);
        return new Listings(Arrays.asList(airings), clnt);
    }

    public Listings getListingsQuickly(Lineup lineup, ListingsFetchRequestOption... options) throws IOException {
        ListingsFetchRequestOption[] allOptions = JsonOptionHelper.generateOptions(options, ListingsFetchRequestOption.class, new DisplayArtworkOption(false));
        return getListings(lineup, allOptions);
    }

    @Override
    public String toString() {
        return "Movie{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", sortedTitle='" + sortedTitle + '\'' +
                ", alternateTitle='" + alternateTitle + '\'' +
                ", description='" + description + '\'' +
                ", editorialDescription='" + editorialDescription + '\'' +
                ", featureDescription='" + featureDescription + '\'' +
                ", noStarRating=" + noStarRating +
                ", starRating=" + starRating +
                ", duration=" + duration +
                ", genre=" + genre +
                ", subGenre='" + subGenre + '\'' +
                ", year='" + year + '\'' +
                ", releaseDate='" + releaseDate + '\'' +
                ", rating='" + rating + '\'' +
                ", usRating='" + usRating + '\'' +
                ", mpaaRating='" + mpaaRating + '\'' +
                ", mpaaReason='" + mpaaReason + '\'' +
                ", imdbId='" + imdbId + '\'' +
                ", officialWebsite='" + officialWebsite + '\'' +
                ", trailerUrl='" + trailerUrl + '\'' +
                ", productionCompany='" + productionCompany + '\'' +
                ", captioned=" + captioned +
                ", educational=" + educational +
                ", blackAndWhite=" + blackAndWhite +
                ", subtitled=" + subtitled +
                ", picture='" + picture + '\'' +
                ", processedCast=" + Arrays.toString(processedCast) +
                '}';
    }
}
