package ca.six43tech.tvmedia.models;

import ca.six43tech.tvmedia.TvMediaClient;
import ca.six43tech.tvmedia.http.TvMediaJsonClient;
import ca.six43tech.tvmedia.http.options.*;
import ca.six43tech.tvmedia.util.JsonOptionHelper;
import com.google.gson.annotations.SerializedName;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.TimeZone;

public class Team extends NonLazyJsonApiObject {
    @SerializedName("teamID") private int id;
    @SerializedName("leagueID") private String leagueId;
    @SerializedName("longName") private String name;
    private String mediumName;
    private String shortName;
    private String logo;

    Team(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public Team(int id, String leagueId, String name, String mediumName, String shortName, String logo) {
        this.id = id;
        this.leagueId = leagueId;
        this.name = name;
        this.mediumName = mediumName;
        this.shortName = shortName;
        this.logo = logo;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Listings getListings(Lineup lineup, ListingsFetchRequestOption... options) throws IOException {
        TvMediaClient clnt = getTvMediaClient();
        List<JsonRequestOption> list = new ArrayList<>(Arrays.asList(options));
        list.add(new TimeZoneOption(TimeZone.getDefault()));
        JsonRequestOption[] allOptions = JsonOptionHelper.generateOptions(list.toArray(new JsonRequestOption[0]), new ApiKeyOption(clnt.getApiKey()), new LineupOption(lineup.getId()));
        Airing[] airings = clnt.convertArrayResponse(clnt.getResponse(String.format("teams/%s/listings", id), TvMediaJsonClient.Action.GET, allOptions), Airing.class);
        return new Listings(Arrays.asList(airings), clnt);
    }

    public Listings getListingsQuickly(Lineup lineup, ListingsFetchRequestOption... options) throws IOException {
        ListingsFetchRequestOption[] allOptions = JsonOptionHelper.generateOptions(options, ListingsFetchRequestOption.class, new DisplayArtworkOption(false));
        return getListings(lineup, allOptions);
    }


    @Override
    public String toString() {
        return "Team{" +
                "id=" + id +
                ", leagueId='" + leagueId + '\'' +
                ", name='" + name + '\'' +
                ", mediumName='" + mediumName + '\'' +
                ", shortName='" + shortName + '\'' +
                ", logo='" + logo + '\'' +
                '}';
    }
}
