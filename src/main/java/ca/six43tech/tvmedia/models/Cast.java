package ca.six43tech.tvmedia.models;

public class Cast extends JsonApiObject {

    private final Person person;
    private String role;

    public Cast(Person person, String role) {
        this.person = person;
        this.role = role;
    }

    public Person getPerson() {
        return person;
    }

    public String getRole() {
        return role;
    }

    @Override
    public String toString() {
        return "Cast{" +
                "person=" + person +
                ", role='" + role + '\'' +
                '}';
    }
}
