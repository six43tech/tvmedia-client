package ca.six43tech.tvmedia.models;

public enum ResponseDetail {
    BRIEF,
    FULL
}
