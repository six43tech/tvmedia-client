package ca.six43tech.tvmedia.models;

import ca.six43tech.tvmedia.TvMediaClient;
import ca.six43tech.tvmedia.http.TvMediaJsonClient;
import ca.six43tech.tvmedia.http.options.*;
import ca.six43tech.tvmedia.util.JsonOptionHelper;
import com.google.gson.annotations.SerializedName;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.TimeZone;

// Why non-lazy? No matter how we access a Station, it's going to be loaded with all optionals included
public class Station extends NonLazyJsonApiObject {
    @SerializedName("stationID") private int id;
    private String name;
    private String callsign;
    private String network;
    private String stationType;
    @SerializedName("NTSC_TSID") private Integer ntscTSID;
    @SerializedName("DTV_TSID") private Integer dtvTSID;
    private String webLink;
    @SerializedName("logoFilename") private String logo;
    @SerializedName("stationHD") private Boolean isHD;

    public Station(int id, String name, String callsign, String network, String stationType, Integer ntscTSID, Integer dtvTSID, String webLink, String logo, Boolean isHD) {
        this.id = id;
        this.name = name;
        this.callsign = callsign;
        this.network = network;
        this.stationType = stationType;
        this.ntscTSID = ntscTSID;
        this.dtvTSID = dtvTSID;
        this.webLink = webLink;
        this.logo = logo;
        this.isHD = isHD;
    }

    public int getId() {
        return id;
    }

    public String getCallsign() {
        return callsign;
    }

    public String getLogo() {
        return logo;
    }

    public String getName() {
        return name;
    }

    public String getNetwork() {
        return network;
    }

    public String getStationType() {
        return stationType;
    }

    public Integer getNtscTSID() {
        return ntscTSID;
    }

    public Integer getDtvTSID() {
        return dtvTSID;
    }

    public String getWebLink() {
        return webLink;
    }

    public Boolean getHD() {
        return isHD;
    }

    public Listings getListings(ListingsFetchRequestOption... options) throws IOException {
        TvMediaClient clnt = getTvMediaClient();
        List<JsonRequestOption> list = new ArrayList<>(Arrays.asList(options));
        list.add(new TimeZoneOption(TimeZone.getDefault()));
        JsonRequestOption[] allOptions = JsonOptionHelper.generateOptions(list.toArray(new JsonRequestOption[0]), new ApiKeyOption(clnt.getApiKey()));
        Airing[] airings = clnt.convertArrayResponse(clnt.getResponse(String.format("stations/%s/listings", id), TvMediaJsonClient.Action.GET, allOptions), Airing.class);
        return new Listings(Arrays.asList(airings), clnt);
    }

    public Listings getListingsQuickly(ListingsFetchRequestOption... options) throws IOException {
        ListingsFetchRequestOption[] allOptions = JsonOptionHelper.generateOptions(options, ListingsFetchRequestOption.class, new DisplayArtworkOption(false));
        return getListings(allOptions);
    }

    @Override
    public String toString() {
        return "Station{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", callsign='" + callsign + '\'' +
                ", network='" + network + '\'' +
                ", stationType='" + stationType + '\'' +
                ", ntscTSID=" + ntscTSID +
                ", dtvTSID=" + dtvTSID +
                ", webLink='" + webLink + '\'' +
                ", logo='" + logo + '\'' +
                ", isHD=" + isHD +
                '}';
    }
}
