package ca.six43tech.tvmedia.models;

import ca.six43tech.tvmedia.TvMediaClient;
import ca.six43tech.tvmedia.http.TvMediaJsonClient;
import ca.six43tech.tvmedia.http.options.*;
import ca.six43tech.tvmedia.util.JsonOptionHelper;
import com.google.gson.annotations.SerializedName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

public class Person extends JsonApiObject {
    static private final Logger LOG = LoggerFactory.getLogger(Person.class);
    static private final String DOB_FMT = "yyyy-MM-dd";

    @SerializedName("personID") private int id;
    private String name;
    private String dateOfBirth;
    @SerializedName("imdbID") private String imdbId;

    public Person(int id, String name, Date dateOfBirth, String imdbId) {
        this.id = id;
        this.name = name;
        this.dateOfBirth = new SimpleDateFormat(DOB_FMT).format(dateOfBirth);
        this.imdbId = imdbId;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Date getDateOfBirth() {
        if(dateOfBirth == null || "0000-00-00".equals(dateOfBirth))
            return null;
        try {
            return new SimpleDateFormat(DOB_FMT).parse(dateOfBirth);
        } catch(Throwable t) {
            LOG.error(String.format("Unparsable DOB value [%s]", dateOfBirth));
            return null;
        }
    }

    public Listings getListings(Lineup lineup, ListingsFetchRequestOption... options) throws IOException {
        TvMediaClient clnt = getTvMediaClient();
        List<JsonRequestOption> list = new ArrayList<>(Arrays.asList(options));
        list.add(new TimeZoneOption(TimeZone.getDefault()));
        JsonRequestOption[] allOptions = JsonOptionHelper.generateOptions(list.toArray(new JsonRequestOption[0]), new ApiKeyOption(clnt.getApiKey()), new LineupOption(lineup.getId()));
        Airing[] airings = clnt.convertArrayResponse(clnt.getResponse(String.format("people/%s/listings", id), TvMediaJsonClient.Action.GET, allOptions), Airing.class);
        return new Listings(Arrays.asList(airings), clnt);
    }

    public Listings getListingsQuickly(Lineup lineup, ListingsFetchRequestOption... options) throws IOException {
        ListingsFetchRequestOption[] allOptions = JsonOptionHelper.generateOptions(options, ListingsFetchRequestOption.class, new DisplayArtworkOption(false));
        return getListings(lineup, allOptions);
    }

    public Listings getMovieListings(Lineup lineup, ListingsFetchRequestOption... options) throws IOException {
        TvMediaClient clnt = getTvMediaClient();
        List<JsonRequestOption> list = new ArrayList<>(Arrays.asList(options));
        list.add(new TimeZoneOption(TimeZone.getDefault()));
        JsonRequestOption[] allOptions = JsonOptionHelper.generateOptions(list.toArray(new JsonRequestOption[0]), new ApiKeyOption(clnt.getApiKey()), new LineupOption(lineup.getId()));
        Airing[] airings = clnt.convertArrayResponse(clnt.getResponse(String.format("people/%s/listings/movies", id), TvMediaJsonClient.Action.GET, allOptions), Airing.class);
        return new Listings(Arrays.asList(airings), clnt);
    }

    public Listings getMovieListingsQuickly(Lineup lineup, ListingsFetchRequestOption... options) throws IOException {
        ListingsFetchRequestOption[] allOptions = JsonOptionHelper.generateOptions(options, ListingsFetchRequestOption.class, new DisplayArtworkOption(false));
        return getMovieListings(lineup, allOptions);
    }

    public Listings getTalkShowListings(Lineup lineup, ListingsFetchRequestOption... options) throws IOException {
        TvMediaClient clnt = getTvMediaClient();
        List<JsonRequestOption> list = new ArrayList<>(Arrays.asList(options));
        list.add(new TimeZoneOption(TimeZone.getDefault()));
        JsonRequestOption[] allOptions = JsonOptionHelper.generateOptions(list.toArray(new JsonRequestOption[0]), new ApiKeyOption(clnt.getApiKey()), new LineupOption(lineup.getId()));
        Airing[] airings = clnt.convertArrayResponse(clnt.getResponse(String.format("people/%s/listings/talkshows", id), TvMediaJsonClient.Action.GET, allOptions), Airing.class);
        return new Listings(Arrays.asList(airings), clnt);
    }

    public Listings getTalkShowListingsQuickly(Lineup lineup, ListingsFetchRequestOption... options) throws IOException {
        ListingsFetchRequestOption[] allOptions = JsonOptionHelper.generateOptions(options, ListingsFetchRequestOption.class, new DisplayArtworkOption(false));
        return getTalkShowListings(lineup, allOptions);
    }

    public String getImdbId() {
        return imdbId;
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", dateOfBirth=" + dateOfBirth +
                ", imdbId='" + imdbId + '\'' +
                '}';
    }
}
