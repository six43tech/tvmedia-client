package ca.six43tech.tvmedia.models;

import ca.six43tech.tvmedia.TvMediaClient;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class Listings extends NonLazyJsonApiObject implements Iterable<Airing> {

    private List<Airing> airings;

    Listings(List<Airing> airings, TvMediaClient clnt) {
        setTvMediaClient(clnt);
        airings.forEach((a)-> a.setTvMediaClient(clnt));
        this.airings = airings;
    }

    @Override
    public Iterator<Airing> iterator() {
        return Collections.unmodifiableList(airings).iterator();
    }
}
