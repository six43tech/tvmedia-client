package ca.six43tech.tvmedia;

import ca.six43tech.tvmedia.http.TvMediaJsonClient;
import ca.six43tech.tvmedia.http.TvMediaJsonClientImpl;
import ca.six43tech.tvmedia.http.options.*;
import ca.six43tech.tvmedia.models.*;
import ca.six43tech.tvmedia.services.JsonConverterService;
import ca.six43tech.tvmedia.services.JsonConverterServiceImpl;
import ca.six43tech.tvmedia.util.JsonOptionHelper;
import ca.six43tech.tvmedia.util.SearchHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class TvMediaClient {
    static private final Logger LOG = LoggerFactory.getLogger(TvMediaClient.class);

    private final TvMediaJsonClient jsonClient;
    private final JsonConverterService jsonConverterService;
    private final String apiKey;

    private final boolean debugMode;

    public TvMediaClient(String apiKey, boolean debugMode, TvMediaJsonClient jsonClient, JsonConverterService jsonConverterService) {
        this.jsonClient = jsonClient;
        this.debugMode = debugMode;
        this.apiKey = apiKey;
        this.jsonConverterService = jsonConverterService;

        if(debugMode)
            this.jsonClient.enableDebugMode();
    }

    public TvMediaClient(String apiKey) {
        this(apiKey, false, new TvMediaJsonClientImpl(apiKey), new JsonConverterServiceImpl());
    }

    public TvMediaClient(String apiKey, boolean debugMode) {
        this(apiKey, debugMode, new TvMediaJsonClientImpl(apiKey), new JsonConverterServiceImpl());
    }

    public String getApiKey() { return apiKey; }

    public Lineup[] getLineupsForZipCode(String zipCode, LineupsLookupRequestOption... options) throws IOException {
        JsonRequestOption[] allOptions = JsonOptionHelper.generateOptions(options, new PostalCodeOption(zipCode), new ApiKeyOption(apiKey));
        return convertArrayResponse(getResponse("lineups", TvMediaJsonClient.Action.GET, allOptions), Lineup.class);
    }

    public Lineup[] getGenericLineups(GenericLineupOption... options) throws IOException {
        JsonRequestOption[] allOptions = JsonOptionHelper.generateOptions(options, new ApiKeyOption(apiKey));
        return convertArrayResponse(getResponse("reference/lineups", TvMediaJsonClient.Action.GET, allOptions), Lineup.class);
    }

    public Lineup getLineup(String id, LineupsLookupRequestOption... options) throws IOException {
        JsonRequestOption[] allOptions = JsonOptionHelper.generateOptions(options, new ApiKeyOption(apiKey));
        return convertResponse(getResponse(String.format("lineups/%s", id), TvMediaJsonClient.Action.GET, allOptions), Lineup.class);
    }

    public Station getStation(String id, StationLookupRequestOption... options) throws IOException {
        JsonRequestOption[] allOptions = JsonOptionHelper.generateOptions(options, new ApiKeyOption(apiKey));
        return convertResponse(getResponse(String.format("stations/%s", id), TvMediaJsonClient.Action.GET, allOptions), Station.class);
    }

    public MovieGenre[] getMovieGenres() throws IOException {
        JsonRequestOption[] allOptions = new JsonRequestOption[] { new ApiKeyOption(apiKey) };
        return convertArrayResponse(getResponse("genres/movies", TvMediaJsonClient.Action.GET, allOptions), MovieGenre.class);
    }

    public ShowGenre[] getShowGenres() throws IOException {
        JsonRequestOption[] allOptions = new JsonRequestOption[] { new ApiKeyOption(apiKey) };
        return convertArrayResponse(getResponse("genres/shows", TvMediaJsonClient.Action.GET, allOptions), ShowGenre.class);
    }

    public SportsGenre[] getSportsGenres() throws IOException {
        JsonRequestOption[] allOptions = new JsonRequestOption[] { new ApiKeyOption(apiKey) };
        return convertArrayResponse(getResponse("genres/sports", TvMediaJsonClient.Action.GET, allOptions), SportsGenre.class);
    }

    public Movie getMovie(int id, MovieLookupRequestOption... options) throws IOException {
        JsonRequestOption[] allOptions = JsonOptionHelper.generateOptions(options, new ApiKeyOption(apiKey));
        return convertResponse(getResponse(String.format("movies/%s", id), TvMediaJsonClient.Action.GET, allOptions), Movie.class);
    }

    public League[] getLeagues() throws IOException {
        return convertArrayResponse(getResponse("leagues", TvMediaJsonClient.Action.GET, new ApiKeyOption(apiKey)), League.class);
    }

    public Movie[] searchMoviesByName(String name, MovieSearchRequestOption... options) throws IOException {
        JsonRequestOption[] allOptions = JsonOptionHelper.generateOptions(options, new ApiKeyOption(apiKey), new NameOption(name));
        String response = getResponse("movies", TvMediaJsonClient.Action.GET, allOptions);
        return convertArrayResponse(SearchHelper.extractMatchesFromSearchResult(response), Movie.class);
    }

    public Movie[] searchMovies(MovieSearchRequestOption... options) throws IOException {
        JsonRequestOption[] allOptions = JsonOptionHelper.generateOptions(options, new ApiKeyOption(apiKey));
        String response = getResponse("movies", TvMediaJsonClient.Action.GET, allOptions);
        return convertArrayResponse(SearchHelper.extractMatchesFromSearchResult(response), Movie.class);
    }

    public Team[] searchTeamsByName(String name) throws IOException {
        String response = getResponse("teams", TvMediaJsonClient.Action.GET, new ApiKeyOption(apiKey), new NameOption(name));
        return convertArrayResponse(SearchHelper.extractMatchesFromSearchResult(response), Team.class);
    }

    public Team getTeam(String id) throws IOException {
        return convertResponse(getResponse(String.format("teams/%s", id), TvMediaJsonClient.Action.GET, new ApiKeyOption(apiKey)), Team.class);
    }

    public Person[] searchPeopleByName(String name) throws IOException {
        String response = getResponse("people", TvMediaJsonClient.Action.GET, new ApiKeyOption(apiKey), new NameOption(name));
        return convertArrayResponse(SearchHelper.extractMatchesFromSearchResult(response), Person.class);
    }

    public Person getPerson(String id) throws IOException {
        return convertResponse(getResponse(String.format("people/%s", id), TvMediaJsonClient.Action.GET, new ApiKeyOption(apiKey)), Person.class);
    }

    public String getResponse(String path, TvMediaJsonClient.Action action, JsonRequestOption... options) throws IOException {
        return jsonClient.getResponse(path, action, options);
    }

    public <T extends JsonApiObject> T convertResponse(String json, Class<T> clazz) {
        T t = jsonConverterService.convertToObject(json, clazz);
        if(t != null)
            t.setTvMediaClient(this);
        return t;
    }

    public <T extends JsonApiObject> T[] convertArrayResponse(String json, Class<T> clazz) {
        T[] array = jsonConverterService.convertToArray(json, clazz);
        if(array != null)
            for(JsonApiObject jao : array)
                if(jao != null)
                    jao.setTvMediaClient(this);
        return array;
    }
}
