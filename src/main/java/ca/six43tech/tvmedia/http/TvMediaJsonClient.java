package ca.six43tech.tvmedia.http;

import ca.six43tech.tvmedia.http.options.JsonRequestOption;

import java.io.IOException;

public interface TvMediaJsonClient {
    enum Action {
        GET
    }

    String getResponse(String resourcePath, Action action, JsonRequestOption... options) throws IOException;
    void enableDebugMode();
    void disableDebugMode();
    boolean isDebugMode();
}
