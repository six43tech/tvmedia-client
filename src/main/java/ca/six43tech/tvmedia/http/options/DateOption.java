package ca.six43tech.tvmedia.http.options;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

abstract public class DateOption implements ListingsFetchRequestOption {

    private Date date;

    public DateOption(Date date) {
        this.date = date;
    }

    @Override
    public String getValue() {
        SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");
        fmt.setTimeZone(TimeZone.getTimeZone("UTC"));
        return fmt.format(date);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if(!(o instanceof JsonRequestOption))
            return false;
        return getName().equals(((JsonRequestOption)o).getName());
    }

    @Override
    public int hashCode() {
        return getName().hashCode();
    }
}
