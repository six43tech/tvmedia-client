package ca.six43tech.tvmedia.http.options;

abstract public class MultiOrSingleValueOption implements ListingsFetchRequestOption {

    private final String[] values;

    public MultiOrSingleValueOption(Object value) {
        this.values = new String[] { value.toString() };
    }

    public MultiOrSingleValueOption(Object... values) {
        if(values != null) {
            this.values = new String[values.length];
            for(int i = 0; i < values.length; ++i)
                this.values[i] = values[i].toString();
        } else
            this.values = new String[0];
    }

    @Override
    public String getValue() {
        return String.join(",", values);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if(!(o instanceof JsonRequestOption))
            return false;
        return getName().equals(((JsonRequestOption)o).getName());
    }

    @Override
    public int hashCode() {
        return getName().hashCode();
    }
}
