package ca.six43tech.tvmedia.http.options;

public class DisplayArtworkOption extends BooleanOption implements ListingsFetchRequestOption {

    public DisplayArtworkOption(boolean value) {
        super(value);
    }

    @Override
    public String getName() {
        return "displayArtwork";
    }
}
