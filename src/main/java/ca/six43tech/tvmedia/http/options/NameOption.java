package ca.six43tech.tvmedia.http.options;

public class NameOption implements MovieSearchRequestOption {

    private String name;

    public NameOption(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return "name";
    }

    @Override
    public String getValue() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if(!(o instanceof JsonRequestOption))
            return false;
        return getName().equals(((JsonRequestOption)o).getName());
    }

    @Override
    public int hashCode() {
        return getName().hashCode();
    }
}
