package ca.six43tech.tvmedia.http.options;

public class LiveOnlyOption extends NoValueOption implements ListingsFetchRequestOption {
    @Override
    public String getName() {
        return "liveOnly";
    }
}
