package ca.six43tech.tvmedia.http.options;

public class PrettyPrintOption extends BooleanOption {

    public PrettyPrintOption(boolean value) {
        super(value);
    }

    @Override
    public String getName() {
        return "pretty";
    }
}
