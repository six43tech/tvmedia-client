package ca.six43tech.tvmedia.http.options;

import java.util.Collection;

public class LeagueOption extends MultiOrSingleValueOption {

    public LeagueOption(String value) {
        super(value);
    }

    public LeagueOption(String[] values) {
        super(values);
    }

    public LeagueOption(Collection<String> values) {
        super(values);
    }

    @Override
    public String getName() {
        return "league";
    }
}
