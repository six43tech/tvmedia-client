package ca.six43tech.tvmedia.http.options;

public class AdultContentOption extends BooleanOption implements ListingsFetchRequestOption, MovieSearchRequestOption {

    public AdultContentOption(boolean value) {
        super(value);
    }

    @Override
    public String getName() {
        return "adultContent";
    }
}
