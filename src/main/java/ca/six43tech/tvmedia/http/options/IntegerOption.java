package ca.six43tech.tvmedia.http.options;

abstract public class IntegerOption implements MovieSearchRequestOption {

    private int value;

    public IntegerOption(int value) {
        this(value, Integer.MIN_VALUE, Integer.MAX_VALUE);
    }

    public IntegerOption(int value, int min, int max) {
        if(value < min || value > max)
            throw new IllegalArgumentException(String.format("Option value is out of range [min: %d, max: %d, val: %d", min, max, value));
    }

    @Override
    public String getValue() {
        return Integer.toString(value);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if(!(o instanceof JsonRequestOption))
            return false;
        return getName().equals(((JsonRequestOption)o).getName());
    }

    @Override
    public int hashCode() {
        return getName().hashCode();
    }
}
