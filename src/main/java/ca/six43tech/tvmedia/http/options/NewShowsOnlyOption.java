package ca.six43tech.tvmedia.http.options;

public class NewShowsOnlyOption extends NoValueOption implements ListingsFetchRequestOption {
    @Override
    public String getName() {
        return "newShowsOnly";
    }
}
