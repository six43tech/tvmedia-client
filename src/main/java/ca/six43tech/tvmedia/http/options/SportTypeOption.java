package ca.six43tech.tvmedia.http.options;

import java.util.Collection;

public class SportTypeOption extends MultiOrSingleValueOption {

    public SportTypeOption(String value) {
        super(value);
    }

    public SportTypeOption(String[] values) {
        super(values);
    }

    public SportTypeOption(Collection<String> values) {
        super(values);
    }

    @Override
    public String getName() {
        return "sporttype";
    }
}
