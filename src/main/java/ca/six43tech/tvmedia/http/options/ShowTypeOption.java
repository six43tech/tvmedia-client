package ca.six43tech.tvmedia.http.options;

import java.util.Collection;

public class ShowTypeOption extends MultiOrSingleValueOption {
    public ShowTypeOption(String value) {
        super(value);
    }

    public ShowTypeOption(String[] values) {
        super(values);
    }

    public ShowTypeOption(Collection<String> values) {
        super(values);
    }

    @Override
    public String getName() {
        return "showType";
    }
}
