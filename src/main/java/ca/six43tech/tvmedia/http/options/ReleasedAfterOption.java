package ca.six43tech.tvmedia.http.options;

import java.util.Date;

public class ReleasedAfterOption extends DateOption implements MovieSearchRequestOption {
    public ReleasedAfterOption(Date date) {
        super(date);
    }

    @Override
    public String getName() {
        return "releasestart";
    }
}
