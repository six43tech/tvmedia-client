package ca.six43tech.tvmedia.http.options;

public interface JsonRequestOption {
    String getName();
    String getValue();
}
