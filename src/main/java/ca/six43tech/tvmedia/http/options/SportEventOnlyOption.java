package ca.six43tech.tvmedia.http.options;

public class SportEventOnlyOption extends NoValueOption implements ListingsFetchRequestOption {
    @Override
    public String getName() {
        return "sportEventOnly";
    }
}
