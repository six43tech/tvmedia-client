package ca.six43tech.tvmedia.http.options;

abstract public class NoValueOption implements JsonRequestOption {
    @Override
    public String getValue() { return ""; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if(!(o instanceof JsonRequestOption))
            return false;
        return getName().equals(((JsonRequestOption)o).getName());
    }

    @Override
    public int hashCode() {
        return getName().hashCode();
    }

}
