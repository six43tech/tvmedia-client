package ca.six43tech.tvmedia.http.options;

import java.util.Collection;

public class SeriesIdOption extends MultiOrSingleValueOption {

    public SeriesIdOption(String value) {
        super(value);
    }

    public SeriesIdOption(String[] values) {
        super(values);
    }

    public SeriesIdOption(Collection<String> values) {
        super(values);
    }

    @Override
    public String getName() {
        return "series";
    }
}
