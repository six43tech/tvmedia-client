package ca.six43tech.tvmedia.http.options;

public class ResultLimitOption extends IntegerOption implements MovieSearchRequestOption {
    public ResultLimitOption(int value) {
        super(value, 1, 200);
    }

    @Override
    public String getName() {
        return "limit";
    }
}
