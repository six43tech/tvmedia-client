package ca.six43tech.tvmedia.http.options;

public class PostalCodeOption implements LineupsLookupRequestOption {

    private String postalCode;

    public PostalCodeOption(String postalCode) {
        this.postalCode = postalCode;
    }

    @Override
    public String getName() {
        return "postalCode";
    }

    @Override
    public String getValue() {
        return postalCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if(!(o instanceof JsonRequestOption))
            return false;
        return getName().equals(((JsonRequestOption)o).getName());
    }

    @Override
    public int hashCode() {
        return getName().hashCode();
    }
}
