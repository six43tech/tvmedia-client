package ca.six43tech.tvmedia.http.options;

import java.util.Date;

public class ReleasedBeforeOption extends DateOption implements MovieSearchRequestOption {
    public ReleasedBeforeOption(Date date) {
        super(date);
    }

    @Override
    public String getName() {
        return "releaseend";
    }
}
