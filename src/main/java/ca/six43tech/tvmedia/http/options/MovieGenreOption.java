package ca.six43tech.tvmedia.http.options;

import java.util.Collection;

public class MovieGenreOption extends MultiOrSingleValueOption implements MovieSearchRequestOption {

    public MovieGenreOption(String value) {
        super(value);
    }

    public MovieGenreOption(String[] values) {
        super(values);
    }

    public MovieGenreOption(Collection<String> values) {
        super(values);
    }

    @Override
    public String getName() {
        return "genre";
    }
}
