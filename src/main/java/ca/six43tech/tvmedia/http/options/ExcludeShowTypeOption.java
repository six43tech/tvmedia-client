package ca.six43tech.tvmedia.http.options;

import java.util.Collection;

public class ExcludeShowTypeOption extends MultiOrSingleValueOption {
    public ExcludeShowTypeOption(String value) {
        super(value);
    }

    public ExcludeShowTypeOption(String[] values) {
        super(values);
    }

    public ExcludeShowTypeOption(Collection<String> values) {
        super(values);
    }

    @Override
    public String getName() {
        return "excludeShowtype";
    }
}
