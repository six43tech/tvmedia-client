package ca.six43tech.tvmedia.http.options;

import java.util.Collection;

public class ExcludeChannelOption extends MultiOrSingleValueOption {
    public ExcludeChannelOption(String value) {
        super(value);
    }

    public ExcludeChannelOption(String[] values) {
        super(values);
    }

    public ExcludeChannelOption(Collection<String> values) {
        super(values);
    }

    @Override
    public String getName() {
        return "excludeChan";
    }
}
