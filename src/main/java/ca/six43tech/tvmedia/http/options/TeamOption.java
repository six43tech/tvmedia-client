package ca.six43tech.tvmedia.http.options;

import java.util.Collection;

public class TeamOption extends MultiOrSingleValueOption {

    public TeamOption(String value) {
        super(value);
    }

    public TeamOption(String[] values) {
        super(values);
    }

    public TeamOption(Collection<String> values) {
        super(values);
    }

    @Override
    public String getName() {
        return "team";
    }
}
