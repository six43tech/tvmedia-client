package ca.six43tech.tvmedia.http.options;

public class DescriptiveVideoOnlyOption extends NoValueOption implements ListingsFetchRequestOption {
    @Override
    public String getName() {
        return "descriptiveVideoOnly";
    }
}
