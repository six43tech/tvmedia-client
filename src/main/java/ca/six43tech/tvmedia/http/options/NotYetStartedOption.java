package ca.six43tech.tvmedia.http.options;

public class NotYetStartedOption extends NoValueOption implements ListingsFetchRequestOption {
    @Override
    public String getName() {
        return "notYetStarted";
    }
}
