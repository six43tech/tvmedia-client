package ca.six43tech.tvmedia.http.options;

public class MaxYearOption extends IntegerOption implements MovieSearchRequestOption {
    public MaxYearOption(int value) {
        super(value);
    }

    @Override
    public String getName() {
        return "maxyear";
    }
}
