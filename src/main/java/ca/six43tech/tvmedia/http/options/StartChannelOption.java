package ca.six43tech.tvmedia.http.options;

public class StartChannelOption implements ListingsFetchRequestOption {

    private final String value;

    public StartChannelOption(String value) { this.value = value; }

    @Override
    public String getName() {
        return "startchan";
    }

    @Override
    public String getValue() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if(!(o instanceof JsonRequestOption))
            return false;
        return getName().equals(((JsonRequestOption)o).getName());
    }

    @Override
    public int hashCode() {
        return getName().hashCode();
    }
}
