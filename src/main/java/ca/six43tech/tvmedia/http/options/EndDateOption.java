package ca.six43tech.tvmedia.http.options;

import java.util.Date;

public class EndDateOption extends DateOption {

    public EndDateOption(Date end) { super(end); }

    @Override
    public String getName() { return "end"; }
}
