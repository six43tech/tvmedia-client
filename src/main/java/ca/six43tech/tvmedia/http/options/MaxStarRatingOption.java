package ca.six43tech.tvmedia.http.options;

public class MaxStarRatingOption extends StarRatingOption implements MovieSearchRequestOption {
    public MaxStarRatingOption(int value) {
        super(value);
    }

    @Override
    public String getName() {
        return "maxstar";
    }
}
