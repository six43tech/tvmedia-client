package ca.six43tech.tvmedia.http.options;

abstract public class BooleanOption implements JsonRequestOption {

    private final boolean value;

    public BooleanOption(boolean value) { this.value = value; }

    @Override
    public String getValue() { return value ? "1" : "0"; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if(!(o instanceof JsonRequestOption))
            return false;
        return getName().equals(((JsonRequestOption)o).getName());
    }

    @Override
    public int hashCode() {
        return getName().hashCode();
    }
}
