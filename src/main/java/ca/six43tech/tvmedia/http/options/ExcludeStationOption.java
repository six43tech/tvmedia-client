package ca.six43tech.tvmedia.http.options;

import java.util.Collection;

public class ExcludeStationOption extends MultiOrSingleValueOption {
    public ExcludeStationOption(String value) {
        super(value);
    }

    public ExcludeStationOption(String[] values) {
        super(values);
    }

    public ExcludeStationOption(Collection<String> values) {
        super(values);
    }

    @Override
    public String getName() {
        return "excludeStation";
    }
}
