package ca.six43tech.tvmedia.http.options;

public class LineupOption implements JsonRequestOption {

    private String id;

    public LineupOption(String id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return "lineup";
    }

    @Override
    public String getValue() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if(!(o instanceof JsonRequestOption))
            return false;
        return getName().equals(((JsonRequestOption)o).getName());
    }

    @Override
    public int hashCode() {
        return getName().hashCode();
    }
}
