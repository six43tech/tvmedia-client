package ca.six43tech.tvmedia.http.options;

public class MinStarRatingOption extends StarRatingOption implements MovieSearchRequestOption {
    public MinStarRatingOption(int value) {
        super(value);
    }

    @Override
    public String getName() {
        return "minstar";
    }
}
