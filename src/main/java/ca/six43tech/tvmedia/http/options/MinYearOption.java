package ca.six43tech.tvmedia.http.options;

public class MinYearOption extends IntegerOption implements MovieSearchRequestOption {
    public MinYearOption(int value) {
        super(value);
    }

    @Override
    public String getName() {
        return "minyear";
    }
}
