package ca.six43tech.tvmedia.http.options;

import ca.six43tech.tvmedia.models.LineupType;

public class LineupTypeOption implements LineupsLookupRequestOption {

    private LineupType type;

    public LineupTypeOption(LineupType type) {
        this.type = type;
    }

    @Override
    public String getName() {
        return "lineupType";
    }

    @Override
    public String getValue() {
        return type.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if(!(o instanceof JsonRequestOption))
            return false;
        return getName().equals(((JsonRequestOption)o).getName());
    }

    @Override
    public int hashCode() {
        return getName().hashCode();
    }
}
