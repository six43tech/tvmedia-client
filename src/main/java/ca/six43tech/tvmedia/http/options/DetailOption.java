package ca.six43tech.tvmedia.http.options;

import ca.six43tech.tvmedia.models.ResponseDetail;

public class DetailOption implements LineupsLookupRequestOption, ListingsFetchRequestOption, GenericLineupOption, MovieLookupRequestOption {

    private String detail;

    public DetailOption(ResponseDetail detail) {
        this.detail = detail.toString().toLowerCase();
    }

    @Override
    public String getName() {
        return "detail";
    }

    @Override
    public String getValue() {
        return detail;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if(!(o instanceof JsonRequestOption))
            return false;
        return getName().equals(((JsonRequestOption)o).getName());
    }

    @Override
    public int hashCode() {
        return getName().hashCode();
    }
}
