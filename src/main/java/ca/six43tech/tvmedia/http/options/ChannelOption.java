package ca.six43tech.tvmedia.http.options;

import java.util.Collection;

public class ChannelOption extends MultiOrSingleValueOption {
    public ChannelOption(String value) {
        super(value);
    }

    public ChannelOption(String[] values) {
        super(values);
    }

    public ChannelOption(Collection<String> values) {
        super(values);
    }

    @Override
    public String getName() {
        return "channel";
    }
}
