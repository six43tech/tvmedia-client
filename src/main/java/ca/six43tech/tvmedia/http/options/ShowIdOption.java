package ca.six43tech.tvmedia.http.options;

import java.util.Collection;

public class ShowIdOption extends MultiOrSingleValueOption {

    public ShowIdOption(String value) {
        super(value);
    }

    public ShowIdOption(String[] values) {
        super(values);
    }

    public ShowIdOption(Collection<String> values) {
        super(values);
    }

    @Override
    public String getName() {
        return "show";
    }
}
