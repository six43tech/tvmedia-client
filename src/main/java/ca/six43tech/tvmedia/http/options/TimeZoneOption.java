package ca.six43tech.tvmedia.http.options;

import java.util.TimeZone;

public class TimeZoneOption implements JsonRequestOption {

    private String tzName;

    public TimeZoneOption(TimeZone tz) { tzName = tz.getID(); }

    @Override
    public String getName() {
        return "timezone";
    }

    @Override
    public String getValue() {
        return tzName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if(!(o instanceof JsonRequestOption))
            return false;
        return getName().equals(((JsonRequestOption)o).getName());
    }

    @Override
    public int hashCode() {
        return getName().hashCode();
    }
}
