package ca.six43tech.tvmedia.http.options;

import java.util.Collection;

public class StationOption extends MultiOrSingleValueOption {
    public StationOption(String value) {
        super(value);
    }

    public StationOption(String[] values) {
        super(values);
    }

    public StationOption(Collection<String> values) {
        super(values);
    }

    @Override
    public String getName() {
        return "station";
    }
}
