package ca.six43tech.tvmedia.http.options;

import java.util.Date;

public class StartDateOption extends DateOption {

    public StartDateOption(Date start) { super(start); }

    @Override
    public String getName() { return "start"; }
}
