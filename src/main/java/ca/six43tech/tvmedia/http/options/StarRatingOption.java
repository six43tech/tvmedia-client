package ca.six43tech.tvmedia.http.options;

abstract public class StarRatingOption extends IntegerOption {
    public StarRatingOption(int value) {
        super(value, 1, 5);
    }
}
