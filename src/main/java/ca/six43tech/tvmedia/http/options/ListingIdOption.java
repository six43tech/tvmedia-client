package ca.six43tech.tvmedia.http.options;

import java.util.Collection;

public class ListingIdOption extends MultiOrSingleValueOption {

    public ListingIdOption(String value) {
        super(value);
    }

    public ListingIdOption(String[] values) {
        super(values);
    }

    public ListingIdOption(Collection<String> values) {
        super(values);
    }

    @Override
    public String getName() {
        return "id";
    }
}
