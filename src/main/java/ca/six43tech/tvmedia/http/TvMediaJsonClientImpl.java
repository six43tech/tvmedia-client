package ca.six43tech.tvmedia.http;

import ca.six43tech.tvmedia.http.options.JsonRequestOption;
import ca.six43tech.tvmedia.http.options.PrettyPrintOption;
import ca.six43tech.tvmedia.services.DebugService;
import ca.six43tech.tvmedia.services.DebugServiceImpl;
import org.apache.http.client.fluent.Executor;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.HashSet;
import java.util.Set;

public class TvMediaJsonClientImpl implements TvMediaJsonClient {
    static private final Logger LOG = LoggerFactory.getLogger(TvMediaJsonClientImpl.class);

    static public final String DEFAULT_URL = "http://api.tvmedia.ca/tv/v4";
    static private final DebugService DEBUG_SERVICE = new DebugServiceImpl();
    static private final Executor EXECUTOR;
    static {
         EXECUTOR = Executor.newInstance(HttpClientBuilder.create().build());
    }

    private final String apiKey;
    private final String baseUrl;
    private final Executor executor;
    private final DebugService debugService;
    private boolean debugMode;

    public TvMediaJsonClientImpl(String apiKey) {
        this(apiKey, DEFAULT_URL, EXECUTOR, DEBUG_SERVICE);
    }

    public TvMediaJsonClientImpl(String apiKey, String baseUrl) { this(apiKey, baseUrl, EXECUTOR, DEBUG_SERVICE); }

    public TvMediaJsonClientImpl(String apiKey, String baseUrl, Executor executor, DebugService debugService) {
        this.apiKey = apiKey;
        this.baseUrl = baseUrl;
        this.executor = executor;
        this.debugService = debugService;
        this.debugMode = false;
        try {
            URL url = new URL(baseUrl);
            String protocol = url.getProtocol();
            if(!"http".equals(protocol) && !"https".equals(protocol)) {
                LOG.error("Encountered invalid service url: {}", url);
                throw new MalformedURLException("TvMedia service is only available via http(s)");
            }
            LOG.debug("Base url set: {}", url);
        } catch(MalformedURLException e) {
            throw new RuntimeException("Invalid service URL", e);
        }
    }

    public String getResponse(String resourcePath, Action action, JsonRequestOption... options) throws IOException {
        URI uri;
        Set<JsonRequestOption> optionSet = new HashSet<>();
        if(options != null)
            for(JsonRequestOption jro : options)
                if(!optionSet.add(jro))
                    throw new IllegalArgumentException(String.format("Request option repeated: %s", jro.getClass().getSimpleName()));

        if(options != null && LOG.isTraceEnabled()) {
            StringBuilder sb = new StringBuilder();
            for(JsonRequestOption jro : optionSet) {
                sb.append(String.format("%s-> %s||", jro.getName(), jro.getValue()));
            }
            LOG.trace("Options received: {}", sb);
        }

        try {
            URIBuilder uriBuilder = new URIBuilder(baseUrl).setPath(String.format("%s/%s", new URI(baseUrl).getPath(), resourcePath));
            if(isDebugMode() && !optionSet.add(new PrettyPrintOption(true)))
                LOG.warn("Honouring caller provided PrettyPrintOption; ignoring debug mode setting");
            for(JsonRequestOption jro : optionSet)
                uriBuilder.addParameter(jro.getName(), jro.getValue());
            uri = uriBuilder.build();
        } catch(URISyntaxException e) {
            throw new IOException(String.format("Invalid URI [%s]", e.getInput()), e);
        }
        LOG.debug("Calling url: {}", uri);
        String result;
        try {
            result = executor.execute(Request.Get(uri.toString())).returnContent().asString();
        } catch(IOException e) {
            LOG.error(String.format("IOError requesting %s", uri), e);
            throw new IOException("API request failed", e);
        }
        if(debugService.isJsonCaptureEnabled()) {
            try {
                LOG.info("JSON response for {} captured to {}", uri, debugService.capture(result));
            } catch(IOException e) {
                LOG.error("Failed to capture JSON response", e);
            }
        }
        return result;
    }

    @Override
    public void enableDebugMode() {
        debugMode = true;
    }

    @Override
    public void disableDebugMode() {
        debugMode = false;
    }

    @Override
    public boolean isDebugMode() {
        return debugMode || debugService.isDebugOverrideEnabled();
    }
}
