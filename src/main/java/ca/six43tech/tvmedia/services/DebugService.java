package ca.six43tech.tvmedia.services;

import java.io.File;
import java.io.IOException;

public interface DebugService {
    boolean isJsonCaptureEnabled();
    void enableJsonCapture();
    void disableJsonCapture();
    File capture(Object data) throws IOException;

    boolean isDebugOverrideEnabled();
}
