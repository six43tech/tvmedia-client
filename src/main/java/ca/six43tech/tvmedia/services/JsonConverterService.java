package ca.six43tech.tvmedia.services;

import ca.six43tech.tvmedia.models.JsonApiObject;

public interface JsonConverterService {
    <T extends JsonApiObject> T convertToObject(String json, Class<T> clazz);
    <T extends JsonApiObject> T[] convertToArray(String json, Class<T> clazz);
}
