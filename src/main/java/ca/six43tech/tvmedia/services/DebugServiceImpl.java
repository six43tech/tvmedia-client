package ca.six43tech.tvmedia.services;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;

public class DebugServiceImpl implements DebugService {
    static private final String SIX43_JSON_CAP_PROP = "six43tech.tvmedia.capture-json";
    static private final String SIX43_JSON_CAP_DIR_PROP = "six43tech.tvmedia.capture-dir";
    private static final String SIX43_DEBUG_OVERRIDE_PROP = "six43tech.tvmedia.debug-override";

    @Override
    public boolean isJsonCaptureEnabled() {
        return Boolean.getBoolean(SIX43_JSON_CAP_PROP);
    }

    @Override
    public void enableJsonCapture() {
        System.setProperty(SIX43_JSON_CAP_PROP, Boolean.TRUE.toString());
    }

    @Override
    public void disableJsonCapture() {
        System.setProperty(SIX43_JSON_CAP_PROP, Boolean.FALSE.toString());
    }

    private File getJsonCaptureDirectory() {
        String value = System.getProperty(SIX43_JSON_CAP_DIR_PROP, System.getProperty("java.io.tmpdir"));
        return new File(value);
    }

    @Override
    public File capture(Object data) throws IOException {
        File f = File.createTempFile("six43_", ".json", getJsonCaptureDirectory());
        Files.write(f.toPath(), data.toString().getBytes(Charset.forName("UTF-8")));
        return f;
    }

    @Override
    public boolean isDebugOverrideEnabled() {
        return Boolean.getBoolean(SIX43_DEBUG_OVERRIDE_PROP);
    }
}
