package ca.six43tech.tvmedia.services;

import ca.six43tech.tvmedia.models.JsonApiObject;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class JsonConverterServiceImpl implements JsonConverterService {

    private final Gson GSON = new GsonBuilder()
                                .setDateFormat("yyyy-MM-dd HH:mm:ss")
                                .create();
    @Override
    public <T extends JsonApiObject> T convertToObject(String json, Class<T> clazz) {
        return GSON.fromJson(json, clazz);
    }

    @Override
    public <T extends JsonApiObject> T[] convertToArray(String json, Class<T> clazz) {
        try {
            return (T[])GSON.fromJson(json, Class.forName("[L" + clazz.getName() + ";"));
        } catch(ClassNotFoundException e) {
            throw new RuntimeException("IMPOSSIBLE ERROR!", e);
        }
    }
}
